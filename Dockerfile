FROM ubuntu:latest as build
ENV TZ=Europe/Berlin
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update \
 && apt-get -qq -y install git \
                           automake \
                           libcurl4-openssl-dev \
                           make \
                           gcc
RUN git clone https://github.com/pooler/cpuminer.git \
 && cd cpuminer \
 && ./autogen.sh \
 && ./configure CFLAGS="-O3" \
 && make

FROM ubuntu:latest
LABEL image.name="epicsoft_cpuminer" \
      image.description="Docker image for cpuminer" \
      maintainer="epicsoft.de" \
      maintainer.name="Alexander Schwarz <schwarz@epicsoft.de>" \
      maintainer.copyright="Copyright 2022 epicsoft.de / Alexander Schwarz" \
      license="MIT"
ENV TZ=Europe/Berlin
RUN apt-get update \
 && apt-get -qq -y install ca-certificates \
                           libcurl4 \
                           libjansson4 \
 && rm -rf /var/lib/apt/lists/*
COPY --from=build /cpuminer/minerd /usr/local/bin/minerd
CMD ["minerd", "--help"]
